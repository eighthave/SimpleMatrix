# Features

## Room Managment
 - [ ] Room Directory
 - [x] Room tag showing  ***(only predefined ones)***
 - [ ] Room tag editing
 - [ ] Search joined rooms

## Room Features
 - [ ] Room user list
 - [ ] Display Room Description
 - [ ] Edit Room Description
 - [ ] Highlights
 - [ ] Pushrules
 - [ ] Send read markers
 - [ ] Display read markers
 - [ ] Sending Invites
 - [ ] Accepting Invites
 - [ ] Typing Notification

## Message Features
 - [ ] E2E
 - [ ] Replies
 - [ ] Attachment uploading
 - [ ] Attachment downloading ***(only Image displaying)***
 - [ ] Send stickers
 - [x] Send formatted messages ***(markdown)***
 - [ ] Rich Text Editor for formatted messages
 - [ ] Redacting

## Other Features
 - [ ] Multiple Accounts ***(planned)***
 - [ ] New user registration ***(not fully implemented)***
 - [ ] VoIP

## Future-specification Features
 - [ ] Communities Display
 - [ ] Communities Creation


This document is based of the official Client Matrix at: https://matrix.org/docs/projects/clients-matrix