Use the Power of Matrix while having a known design.

A Matrix Client written in the Style of Telegram/Signal/Whatsapp.
With its focus to simplify your experience with the Matrix (https://matrix.org)

Features include:

* Instantly share messages, images, videos and files of any kind within groups of any size
* Communicate with users anywhere in the Matrix.org ecosystem - not just SimpleMatrix users!
* Highly scalable - supports hundreds of rooms and thousands of users
* Fully synchronised message history across multiple devices and browsers

Note that SimpleMatrix F-Droid release does currently include GCM/FCM for notifications
but does not activate any request if google is missing on the device - instead it
will keep syncing in the background. If you find that the ongoing background
sync is using too much battery, you can add a delay or change the timeout of the
sync.

For developers:

* SimpleMatrix is a Matrix client - built on the Matrix.org open standard and ecosystem, providing interoperability with all other Matrix compatible apps, servers and integrations
* Entirely open sourced under the permissive AGPL-v3 - get the code from <a href="https://gitlab.com/Nordgedanken/simplematrix"></a>. Pull requests welcome!
* Trivially extensible via the open Matrix Client-Server API (<a href="https://matrix.org/docs/spec"></a>)
* Run your own server! You can use the default matrix.org server or run your own Matrix home server (e.g. <a href="https://matrix.org/docs/projects/server/synapse.html"></a>)

Coming soon:

* A lot of missing Matrix Features. The SimpleMatrix client is at the very begining

'''AntiFeatures:'''

* NonFreeNet - Currently the integration server is not configurable in this client and linked to the non-free New Vector implementation. Free alternatives exists but supporting them in Riot Android is still being worked on.
