/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.os.Handler
import android.os.Looper
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.orhanobut.logger.Logger
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.tls.internal.TlsUtil.localhost
import org.hamcrest.core.IsNull
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginActivityTest {
    private var mActivity: MatrixAccountActivity? = null

    // Create a MockWebServer. These are lean enough that you can create a new
    // instance for every unit test.
    @get:Rule
    val server = MockWebServer()

    @get:Rule
    var mActivityRule: ActivityTestRule<MatrixAccountActivity> = ActivityTestRule(MatrixAccountActivity::class.java)

    @Before
    fun setUp() {
        mActivity = mActivityRule.activity
        ViewMatchers.assertThat(mActivity, IsNull.notNullValue())
    }

    @Test
    fun matrixIDInputShouldWork() {
        val username = "MTRNord"
        // Type text
        onView(withId(R.id.matrix_id))
                .perform(typeText(username), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.matrix_id))
                .check(matches(withText(username)))
    }

    @Test
    fun passwordInputShouldWork() {
        val password = "Test1234"
        // Type text
        onView(withId(R.id.password))
                .perform(typeText(password), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.password))
                .check(matches(withText(password)))
    }

    @Test
    fun serverInputShouldWork() {
        val server = "matrix.ffslfl.net"
        // Type text
        onView(withId(R.id.server))
                .perform(typeText(server), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.server))
                .check(matches(withText(server)))
    }

    @Test
    fun loginShouldWork() {
        server.useHttps(localhost().sslSocketFactory(), false)

        val baseURL = server.url("/_matrix/client/r0/login")
        //server.start()

        val body = "{\n" +
                "  \"user_id\": \"@MTRNord:"+baseURL.host()+"\",\n" +
                "  \"access_token\": \"abc123\",\n" +
                "  \"device_id\": \"GHTYAJCE\"\n" +
                "}"
        server.enqueue(MockResponse().setBody(body).setResponseCode(200))

        val username = "MTRNord"
        val password = "dummy"
        val serverURL = baseURL.scheme() + "://" + baseURL.host() + ":" + baseURL.port()

        // Type server address
        onView(withId(R.id.server))
                .perform(scrollTo(), typeText(serverURL), closeSoftKeyboard())

        // Check that the server address was changed.
        onView(withId(R.id.server))
                .check(matches(withText(serverURL)))

        // Type matrix ID
        onView(withId(R.id.matrix_id))
                .perform(scrollTo(), typeText(username), closeSoftKeyboard())

        // Check that the matrix ID was changed.
        onView(withId(R.id.matrix_id))
                .check(matches(withText(username)))

        // Type password
        onView(withId(R.id.password))
                .perform(scrollTo(), typeText(password), closeSoftKeyboard())

        // Check that the password was changed.
        onView(withId(R.id.password))
                .check(matches(withText(password)))

        Handler(Looper.getMainLooper()).post {
            mActivity?.login(mActivity?.findViewById(R.id.sign_in_button))
        }

        val loginRequest = server.takeRequest()
        Logger.d("[loginShouldWork] " + loginRequest.path)
        assertEquals("/_matrix/client/r0/login", loginRequest.path)
        server.shutdown()
    }
}