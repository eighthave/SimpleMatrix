package blog.nordgedanken.simplematrix.fcm

import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.messaging.FirebaseMessaging

class FCMHelper {
    fun init(context: Context) {
        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
            FirebaseMessaging.getInstance().isAutoInitEnabled = true
        }
    }
}