package blog.nordgedanken.simplematrix.fcm;

/**
 * Created by MTRNord on 23.12.2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class SetPusher implements Serializable {

    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("app_display_name")
    @Expose
    private String appDisplayName;
    @SerializedName("device_display_name")
    @Expose
    private String deviceDisplayName;
    @SerializedName("profile_tag")
    @Expose
    private String profileTag;
    @SerializedName("app_id")
    @Expose
    private String appId;
    @SerializedName("pushkey")
    @Expose
    private String pushkey;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("append")
    @Expose
    private Boolean append;
    private final static long serialVersionUID = 5004982755727313375L;

    /**
     * No args constructor for use in serialization
     */
    public SetPusher() {
    }

    /**
     * @param append
     * @param deviceDisplayName
     * @param appId
     * @param data
     * @param pushkey
     * @param profileTag
     * @param appDisplayName
     * @param lang
     * @param kind
     */
    public SetPusher(String lang, String kind, String appDisplayName, String deviceDisplayName, String profileTag, String appId, String pushkey, Data data, Boolean append) {
        super();
        this.lang = lang;
        this.kind = kind;
        this.appDisplayName = appDisplayName;
        this.deviceDisplayName = deviceDisplayName;
        this.profileTag = profileTag;
        this.appId = appId;
        this.pushkey = pushkey;
        this.data = data;
        this.append = append;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getAppDisplayName() {
        return appDisplayName;
    }

    public void setAppDisplayName(String appDisplayName) {
        this.appDisplayName = appDisplayName;
    }

    public String getDeviceDisplayName() {
        return deviceDisplayName;
    }

    public void setDeviceDisplayName(String deviceDisplayName) {
        this.deviceDisplayName = deviceDisplayName;
    }

    public String getProfileTag() {
        return profileTag;
    }

    public void setProfileTag(String profileTag) {
        this.profileTag = profileTag;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPushkey() {
        return pushkey;
    }

    public void setPushkey(String pushkey) {
        this.pushkey = pushkey;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Boolean getAppend() {
        return append;
    }

    public void setAppend(Boolean append) {
        this.append = append;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lang", lang).append("kind", kind).append("appDisplayName", appDisplayName).append("deviceDisplayName", deviceDisplayName).append("profileTag", profileTag).append("appId", appId).append("pushkey", pushkey).append("data", data).append("append", append).toString();
    }

}


