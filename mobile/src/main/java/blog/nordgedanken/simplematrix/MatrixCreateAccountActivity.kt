/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import blog.nordgedanken.simplematrix.accounts.MatrixAccountAuthenticator

/**
 *
 * TODO DOCUMENT WHEN IMPLEMENTED!
 *
 * @author Marcel Radzio
 */
@SuppressLint("Registered")
class MatrixCreateAccountActivity : AppCompatActivity() {

  private var accountType: String? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_create_account)

    accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE)

  }

  fun createAccount(view: View) {
    val userId = (findViewById<View>(R.id.matrix_id) as EditText).text.toString()
    val passWd = (findViewById<View>(R.id.password) as EditText).text.toString()
    val server = (findViewById<View>(R.id.server) as EditText).text.toString()

    // TODO implement account creation
    // val authToken = MatrixClient.createAccount(server, userId, userId, passWd)

    val authToken = ""
    if (authToken.isEmpty()) {
      (findViewById<View>(R.id.sign_in_button) as Button).error = "Account couldn't be registered, please try again."
    }

    val data = Bundle()
    data.putString(AccountManager.KEY_ACCOUNT_NAME, userId)
    data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
    data.putString(AccountManager.KEY_AUTHTOKEN, authToken)
    data.putString(MatrixAccountAuthenticator.SERVER, server)

    val result = Intent()
    result.putExtras(data)

    setResult(Activity.RESULT_OK, result)
    finish()
  }
}