package blog.nordgedanken.simplematrix.chatView

import android.Manifest
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageButton
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.State
import blog.nordgedanken.simplematrix.chatView.recyclerView.MessageList
import blog.nordgedanken.simplematrix.chatView.recyclerView.PagingMessageController
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.matrix.sync.processing.JoinedRooms
import blog.nordgedanken.simplematrix.data.view.Message
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.data.view.MessageViewModel
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.databinding.ActivityChatRoomBinding
import blog.nordgedanken.simplematrix.utils.MediaStoreHelper
import blog.nordgedanken.simplematrix.utils.db.DBHelpers
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.doesRoomExistInDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getMessageCountByRoomIDFromDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getMessagesByRoomIDFromDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getRoomByID
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.saveMessagesToDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.updateMessageIDInDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.updateMessageSendingStatusInDB
import blog.nordgedanken.simplematrix.utils.ui.SlideAnimations
import com.airbnb.deeplinkdispatch.DeepLink
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.orhanobut.logger.Logger
import io.kamax.matrix.hs._MatrixRoom
import io.kamax.matrix.room.MatrixRoomMessageChunkOptions
import io.kamax.matrix.room._MatrixRoomMessageChunkOptions
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import kotlinx.android.synthetic.main.content_chat_room.*
import okhttp3.HttpUrl
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.koin.android.ext.android.inject
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import permissions.dispatcher.RuntimePermissions
import java.io.FileNotFoundException
import java.util.*

@RuntimePermissions
@DeepLink("https://matrix.to/#/{roomID}")
class ChatRoom : AppCompatActivity() {

    private lateinit var room: RoomFull
    private var rawRoom: _MatrixRoom? = null

    private val state: State by inject()

    private lateinit var recyclerView: MessageList
    private val pagingController: PagingMessageController by lazy { PagingMessageController(this@ChatRoom) }
    private val viewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this).get(MessageViewModel::class.java)
    }

    private val slideAnimationsUtil = SlideAnimations()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("User opened a ChatRoom").build()
        )

        /**
         * Setup DataBinding UI
         */
        val activityBinding = DataBindingUtil.setContentView<ActivityChatRoomBinding>(
                this,
                R.layout.activity_chat_room
        )

        val bgImageUrl = PreferenceManager.getDefaultSharedPreferences(this).getString("room_bg_image_url", "")
        if (bgImageUrl != null && bgImageUrl != "") {
            loadBGImageWithPermissionCheck(activityBinding, bgImageUrl)
        }

        setSupportActionBar(activityBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView = activityBinding.content.recyclerView
        /**
         * Setup epoxy list
         */
        setupEpoxy()

        val roomID = if (intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false)) {
            val parameters = intent.extras
            val rawFragment = parameters?.getString("deep_link_uri")?.split('#', limit = 2)!![1]
            val httpurl = HttpUrl.parse("https://matrix.to$rawFragment")
            if (httpurl?.fragment() == null && httpurl?.query() != null) {
                httpurl.pathSegments()[0]
            } else {
                rawFragment.trimStart('/').split("\\?.*".toRegex())[0]
            }
        } else {
            intent.getStringExtra("roomID")
        }
        setupInput(activityBinding.content.editText, activityBinding.content.sendButton)

        viewModel.messageList(roomID!!).observe(this@ChatRoom, Observer {
            pagingController.submitList(it)

            pagingController.requestForcedModelBuild()
        })
        pagingController.requestForcedModelBuild()
        doAsync {
            rawRoom = MatrixClient.client?.getRoom(roomID)

            if (doesRoomExistInDB(roomID)) {
                room = getRoomByID(roomID)

                runOnUiThread {
                    activityBinding.room = room
                }
            } else {
                val viaServer = if (intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false)) {
                    val parameters = intent.extras
                    when {
                        roomID.startsWith("!") -> {
                            val httpurl = HttpUrl.parse("https://matrix.to" + parameters?.getString("deep_link_uri")?.split('#', limit = 2)!![1])
                            httpurl?.queryParameterValues("via")
                        }
                        roomID.startsWith("#") -> {
                            val fakeUrlToParseQuery = HttpUrl.parse("https://" + parameters?.getString("deep_link_uri")?.split(':', limit = 3)!![2])
                            fakeUrlToParseQuery?.queryParameterValues("via")
                        }
                        else -> mutableListOf<String>()
                    }
                } else {
                    null
                }
                if (viaServer != null) {
                    val joinError = rawRoom?.tryJoin(viaServer)
                    Logger.d(joinError)
                    if (joinError?.isEmpty!!) {
                        room = getRoomByID(roomID)

                        runOnUiThread {
                            activityBinding.room = room
                        }
                    } else {
                        Logger.e("Some Join Error happened: ${joinError.get()}")
                    }
                } else {
                    Logger.e("Some Join Error happened: No via Servers defined}")
                }

            }

            if (getMessageCountByRoomIDFromDB(room.room?.id!!) <= 10) {
                doAsync {
                    loadMore()
                }
            }

            // Depends on existing roomID!
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (isRecyclerViewAtTop()) {
                        doAsync {
                            loadMore()
                        }
                    }
                }

                private fun isRecyclerViewAtTop(): Boolean {
                    if (recyclerView.childCount == 0)
                        return true
                    val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                    if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.adapter?.itemCount!! - 1)
                        return true

                    return false
                }
            })
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated method
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun loadBGImage(activityBinding: ActivityChatRoomBinding, bgImageUrl: String) {
        val bgImageBitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(Uri.parse(bgImageUrl)))
        val bgImageDrawable = BitmapDrawable(resources, bgImageBitmap)
        activityBinding.backgroundImageUrl = bgImageDrawable
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun showRationaleForStorage(request: PermissionRequest) {
        // NOTE: Show a rationale to explain why the permission is needed, e.g. with a dialog.
        // Call proceed() or cancel() on the provided PermissionRequest to continue or abort
        showRationaleDialog(R.string.storage_permission_reason, request)
    }

    private fun showRationaleDialog(@StringRes messageResID: Int, request: PermissionRequest) {
        AlertDialog.Builder(this)
                .setPositiveButton("Allow") { _, _ -> request.proceed() }
                .setCancelable(false)
                .setMessage(messageResID)
                .show()
    }

    private fun loadMore() {
        if (room.room?.id!! !in state.loadingBacklogOf) {
            state.loadingBacklogOf.add(room.room?.id!!)
            if (room.room?.prevBatchToken != "") {
                val roomChunkOptions = MatrixRoomMessageChunkOptions.Builder()
                        .setDirection(_MatrixRoomMessageChunkOptions.Direction.Backward)
                        .setFromToken(room.room?.prevBatchToken)
                        .setLimit(10)
                        .get()
                val response = rawRoom?.getMessages(roomChunkOptions)

                val rooms = mutableListOf<RoomFull>()

                room.room?.prevBatchToken = response?.endToken!!
                val messages = getMessagesByRoomIDFromDB(room.room?.id!!).toMutableList()

                val timelineEvents = response.events
                if (timelineEvents.isNotEmpty()) {
                    // We can save assume that the room exists in the DB
                    JoinedRooms().processTimeline(timelineEvents, room, messages)
                    saveMessagesToDB(messages.filterNotNull())
                    rooms.add(room)
                    DBHelpers.saveRoomToDB(rooms)
                    room = getRoomByID(room.room?.id!!)
                }
            }
            state.loadingBacklogOf.remove(room.room?.id!!)
        }
    }

    private fun setupEpoxy() {
        recyclerView.setController(pagingController)

        pagingController.adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (isRecyclerViewAtBottom()) {
                    val recyclerLayoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    recyclerLayoutManager.scrollToPosition(0)
                }
            }

            private fun isRecyclerViewAtBottom(): Boolean {
                if (recyclerView.childCount == 0)
                    return true
                val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == 0)
                    return true

                return false
            }
        })

        // Init swipe actions
        /*EpoxyTouchHelper.initSwiping(recyclerView)
                .leftAndRight() // Which directions to allow
                .forAllModels()
                .andCallbacks(object : EpoxyTouchHelper.SwipeCallbacks<EpoxyModel<*>?>() {
                    override fun onSwipeCompleted(
                            model: EpoxyModel<*>?,
                            itemView: View?,
                            position: Int,
                            direction: Int
                    ) {
                        Logger.d("Swipe")
                        when (model) {
                            is IncomingTextMessageLayoutElementBindingModel_ -> Logger.d("Incoming")
                            is OutgoingTextMessageLayoutElementBindingModel_ -> Logger.d("Outgoing")
                        }
                        pagingController.requestForcedModelBuild()
                    }

                })*/
    }

    private fun setupInput(editText: EditText?, sendButton: ImageButton?) {
        editText?.setOnEditorActionListener { v, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEND -> {
                    sendMessage(v.text)
                    v.text = ""
                    true
                }
                else -> false
            }
        }

        sendButton?.setOnClickListener {
            sendMessage(editText?.text)
            editText?.text?.clear()
        }

        attachmentButton?.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            val targetUri = data?.data!!
            doAsync {
                try {
                    sendImageMessage(targetUri)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    @Throws(FileNotFoundException::class)
    private fun sendImageMessage(targetUri: Uri) {
        val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(targetUri))
        val filenameSplit = MediaStoreHelper.getFilePath(this, targetUri)?.split("/")
        val filename = filenameSplit?.get(filenameSplit.size - 1)!!
        val mimeType = MediaStoreHelper.getMimeType(this, targetUri)
        val mxurl = MatrixClient.client?.putMedia(
                MediaStoreHelper.readBytesFromAndroidImageURL(this, targetUri),
                mimeType,
                filename
        )

        val message = Message()
        val messageFull = MessageFull()
        messageFull.message = message

        messageFull.message?.userID = MatrixClient.id
        messageFull.message?.roomID = room.room?.id

        messageFull.message?.image = mxurl!!
        messageFull.message?.imageWidth = bitmap.width
        messageFull.message?.imageHeight = bitmap.height

        val randomID = Math.random().toString()
        messageFull.message?.id = randomID

        messageFull.message?.createdAt = Date()
        messageFull.message?.sendingInProcess = true

        doAsync {
            val messages = arrayListOf(messageFull)
            saveMessagesToDB(messages)

            val info = JsonObject()
            info.add("size", JsonPrimitive(MediaStoreHelper.getFileSize(this@ChatRoom, targetUri)))
            info.add("w", JsonPrimitive(messageFull.message?.imageWidth))
            info.add("h", JsonPrimitive(messageFull.message?.imageHeight))
            info.add("mimetype", JsonPrimitive(mimeType))

            val event = JsonObject()
            event.add("body", JsonPrimitive(filename))
            event.add("info", info)
            event.add("url", JsonPrimitive(messageFull.message?.image!!))
            event.add("msgtype", JsonPrimitive("m.image"))

            val id = rawRoom?.sendEvent("m.room.message", event)

            updateMessageIDInDB(messageFull.message?.id!!, id!!)
            updateMessageSendingStatusInDB(id, false)
        }
    }

    private fun sendMessage(text: CharSequence?) {
        val textS = text.toString()
        if (text != null && textS != "") {
            val parsedText = parseMarkdown(textS)

            val message = Message()
            val messageFull = MessageFull()
            messageFull.message = message

            messageFull.message?.userID = MatrixClient.id
            messageFull.message?.roomID = room.room?.id

            messageFull.message?.text = textS
            messageFull.message?.textHTML = parsedText

            val randomID = Math.random().toString()
            messageFull.message?.id = randomID

            messageFull.message?.createdAt = Date()
            messageFull.message?.sendingInProcess = true

            doAsync {
                val messages = arrayListOf(messageFull)
                saveMessagesToDB(messages)
                val id = rawRoom?.sendFormattedText(parsedText, textS)

                updateMessageIDInDB(messageFull.message?.id!!, id!!)
                updateMessageSendingStatusInDB(id, false)
            }
        }

    }

    override fun onBackPressed() {
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("User closed a ChatRoom").build()
        )
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.chatroom_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun parseMarkdown(input: String): String {
        val parser = Parser.builder()
                .build()
        val document = parser.parse(input)
        val renderer = HtmlRenderer.builder()
                .softbreak("<br>")
                .build()
        val renderedMD = renderer.render(document)
        val whitelist = Whitelist.none()
        whitelist.addTags("font", "del", "h1", "h2", "h3", "h4", "h5", "h6", "blockquote", "p", "a", "ul", "ol", "sup", "sub", "li", "b", "i", "u", "strong", "em", "strike", "code", "hr", "br", "div", "table", "thead", "tbody", "tr", "th", "td", "caption", "pre", "span", "img", "mx-reply")
        var safeRenderedMD = Jsoup.clean(renderedMD, whitelist)
        safeRenderedMD = safeRenderedMD.replace("\n", "<br>")

        return safeRenderedMD
    }
}