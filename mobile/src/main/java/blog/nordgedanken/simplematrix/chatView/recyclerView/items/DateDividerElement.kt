package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.R
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.date_divider_element)
abstract class DateDividerElement : EpoxyModelWithHolder<DateDividerElement.Holder>() {

    var date: Date? = null

    override fun bind(holder: Holder) {
        if (date != null) {
            val formatter = SimpleDateFormat("EEEE・HH:mm", Locale.getDefault())
            val dateS = formatter.format(date)
            setPrecomputedText(holder.dateView, dateS)
        }
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {
        lateinit var dateView: TextView

        override fun bindView(itemView: View) {
            dateView = itemView.findViewById(R.id.dateView)
        }

    }
}