package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.text.Spanned
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.GlideRequests
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.User
import blog.nordgedanken.simplematrix.utils.ImageLoader
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.incoming_image_message_layout_element)
abstract class IncomingImageMessage : EpoxyModelWithHolder<IncomingImageMessage.Holder>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    var avatarImageURL: String? = null
    var imageURL: String? = null
    var user: User? = null
    var content: Spanned? = null
    var username: String? = null
    var createdAt: Date? = null

    override fun bind(holder: Holder) {
        holder.avatarImage.setOnClickListener(clickListener)
        if (avatarImageURL != null && user != null) {
            holder.imageLoader.loadImage(holder.avatarImage, avatarImageURL!!, user!!)
        }
        user = null

        if (imageURL != null) {
            holder.imageLoader.loadImage(holder.image, imageURL!!)
        }
        if (content != null) {
            holder.image.contentDescription = content
        }
        setPrecomputedText(holder.username, username!!)
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {

        lateinit var glide: GlideRequests
        lateinit var imageLoader: ImageLoader

        lateinit var avatarImage: CircleImageView
        lateinit var image: ImageView
        lateinit var username: TextView

        override fun bindView(itemView: View) {
            glide = GlideApp.with(itemView)
            imageLoader = ImageLoader(itemView.context, glide)

            avatarImage = itemView.findViewById(R.id.circleImageView)
            image = itemView.findViewById(R.id.image)
            username = itemView.findViewById(R.id.username)

        }

    }
}