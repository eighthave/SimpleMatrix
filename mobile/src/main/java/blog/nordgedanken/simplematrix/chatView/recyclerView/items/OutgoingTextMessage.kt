package blog.nordgedanken.simplematrix.chatView.recyclerView.items

import android.text.Spanned
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import blog.nordgedanken.simplematrix.R
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.VisibilityState
import com.github.ybq.android.spinkit.SpinKitView
import java.util.*

/**
 * Created by MTRNord on 19.02.2019.
 */
@EpoxyModelClass(layout = R.layout.outgoing_text_message_layout_element)
abstract class OutgoingTextMessage : EpoxyModelWithHolder<OutgoingTextMessage.Holder>() {

    var content: Spanned? = null
    var createdAt: Date? = null

    @VisibilityState.Visibility
    var sending: Int? = null

    override fun bind(holder: Holder) {
        if (sending != null) {
            holder.spinner.visibility = sending!!
        }
        setPrecomputedText(holder.content, content!!)
    }

    private fun setPrecomputedText(view: TextView, text: CharSequence?) {
        if (text != null) {
            val params = TextViewCompat.getTextMetricsParams(view)
            (view as AppCompatTextView).setTextFuture(
                    PrecomputedTextCompat.getTextFuture(text, params, null))
        }
    }

    class Holder : EpoxyHolder() {

        lateinit var content: TextView
        lateinit var spinner: SpinKitView

        override fun bindView(itemView: View) {
            content = itemView.findViewById(R.id.content)
            spinner = itemView.findViewById(R.id.spin_kit)

        }

    }
}