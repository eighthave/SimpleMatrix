/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.matrix

import androidx.room.*

/**
 * Created by MTRNord on 01.09.2018.
 */
@Dao
interface SyncDao {
    @get:Query("SELECT * FROM sync")
    val all: List<Sync>

    @Insert
    fun insertAll(vararg syncs: Sync)

    @Update
    fun updateAll(vararg syncs: Sync)

    @Delete
    fun delete(sync: Sync)

    @Query("DELETE FROM sync")
    fun nukeTable()
}