package blog.nordgedanken.simplematrix.data.matrix.sync

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startForegroundService
import androidx.lifecycle.MutableLiveData
import blog.nordgedanken.simplematrix.data.matrix.sync.backgroundSync.BackgroundSyncService
import blog.nordgedanken.simplematrix.data.matrix.sync.initialSync.InitialSyncService

object SyncStarter {
    val initialSyncDone = MutableLiveData<Boolean>()

    fun startInitialSync(context: Context) {
        // Start Sync
        Intent(context, InitialSyncService::class.java).also { intent ->
            startForegroundService(context, intent)
        }
    }

    fun startSync(context: Context) {
        // Start Sync
        Intent(context, BackgroundSyncService::class.java).also { intent ->
            startForegroundService(context, intent)
        }

    }
}