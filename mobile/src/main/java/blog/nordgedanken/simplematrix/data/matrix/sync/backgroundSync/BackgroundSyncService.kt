package blog.nordgedanken.simplematrix.data.matrix.sync.backgroundSync

import android.accounts.AccountManager
import android.app.IntentService
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.content.ContextCompat
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.App
import blog.nordgedanken.simplematrix.MainActivity
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.data.matrix.sync.processing.SyncProcessing
import blog.nordgedanken.simplematrix.utils.Connectivity
import blog.nordgedanken.simplematrix.utils.Notification
import blog.nordgedanken.simplematrix.utils.removeAccount
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.orhanobut.logger.Logger
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client.regular.SyncOptions
import io.kamax.matrix.json.GsonUtil
import org.koin.android.ext.android.inject

const val ONGOING_NOTIFICATION_ID = 9999
/**
 * Created by MTRNord on 15.02.2019.
 */
class BackgroundSyncService : IntentService("BG_Sync_Service") {

    private val account: Account by inject()
    private val db: AppDatabase by inject()
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate() {
        super.onCreate()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val notification = Notification.buildForegroundServiceNotification(this, R.string.backgroundSync)

        this.startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        loop()
    }

    private fun sync(timeout: Long, nextBatchToken: String): _SyncData? {
        with(SyncOptions.build()) {
            setSince(nextBatchToken)
            setTimeout(timeout)
            val timeline = JsonObject()
            timeline.add("limit", JsonPrimitive(20))

            val roomFilter = JsonObject()
            roomFilter.add("timeline", timeline)

            val filter = JsonObject()
            filter.add("room", roomFilter)

            val jsonString = GsonUtil.get().toJson(filter)
            setFilter(jsonString)
            return account.client?.sync(get())
        }
    }

    // TODO return enum
    private fun actualWork(db: AppDatabase): Boolean {
        val localInitialSyncDone = SyncStarter.initialSyncDone.value ?: true
        if (!localInitialSyncDone) {
            Logger.d("InitialSync not done")
            return true
        }

        // Get Sync Token from DB
        Logger.d("nextup Sync")
        val all = db.syncDao().all
        if (all.isEmpty()) return true
        val nextBatchToken = all[0].syncToken

        val syncData = try {
            sync(3000, nextBatchToken)
        } catch (e: MatrixClientRequestException) {
            if (e.error.isPresent) {
                val error = e.error.get()
                if (error.errcode == "M_UNKNOWN_TOKEN") {
                    MatrixClient.client = null
                    val am = AccountManager.get(this)
                    val accounts = am?.getAccountsByType("org.matrix")
                    // TODO fix when using multiple accounts
                    removeAccount(account = accounts!![0], activity = null, handler = null, callback = null, accountManager = am)

                    // Let the User login again
                    val mainActivityIntent = Intent(this, MainActivity::class.java)
                    ContextCompat.startActivity(this, mainActivityIntent, null)
                    return false
                }
                Logger.d("ErrorCode: " + error.errcode + "\nErrorMessage: " + error.error)
            }
            null
        } catch (e: Exception) {
            Logger.d("Some error happened: ${e.message}")
            null
        }

        if (syncData !== null) {
            SyncProcessing(syncData)
            // Save the next Batch token for the next sync iteration
            if (db.syncDao().all.count() == 1) {
                val sync = db.syncDao().all[0]
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().updateAll(sync)
            } else {
                val sync = Sync()
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().insertAll(sync)
            }
            return false
        } else {
            return false
        }
    }

    private fun loop() {
        Logger.d("Doing background sync")
        while (true) {
            Logger.d("while")
            if (Connectivity.isConnected(this)) {
                Logger.d("connected")
                val critical = actualWork(db)
                if (critical) {
                    Logger.e("Critical Error")
                    stopSelf()
                }
                // TODO delay longer in case of empty data
                if (App.appState == "background") {
                    val minutesS = sharedPreferences.getString("bg_sync_time", "5")
                    val minutes = minutesS?.toLong()!!
                    Thread.sleep(minutes * 1000 * 60) // 5min
                } else {
                    val minutesS = sharedPreferences.getString("fg_sync_time", "1")
                    val minutes = minutesS?.toLong()!!
                    Thread.sleep(minutes * 1000 * 60) // 1min
                }
            }
        }
    }
}