package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import com.google.gson.JsonObject
import com.orhanobut.logger.Logger
import io.kamax.matrix.event._MatrixEvent
import io.kamax.matrix.json.InvalidJsonException
import io.kamax.matrix.json.MatrixJsonEventFactory

/**
 * Parse the different Events to their correct type
 * @author Marcel Radzio
 */
fun parseEvent(json: JsonObject): _MatrixEvent? {
    return try {
        MatrixJsonEventFactory.get(json)
    } catch (e: InvalidJsonException) {
        //Logger.d("Malformed event: $json", e)
        null
    } catch (e: NullPointerException) {
        //Logger.d("Malformed event: $json", e)
        null
    } catch (e: NumberFormatException) {
        //Logger.d("Malformed event: $json", e)
        null
    } catch (e: NoClassDefFoundError) {
        //Logger.e(e.message!!, e.stackTrace)
        null
    } catch (e: Exception) {
        Logger.e(e.message!!, e.stackTrace)
        null
    }
}