/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import android.text.Spanned
import androidx.annotation.NonNull
import androidx.core.text.HtmlCompat
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "messages")
class Message {

  @PrimaryKey
  @NonNull
  var id: String = ""
    @ColumnInfo(name = "user_id") var userID: String? = null
    @ColumnInfo(name = "room_id") var roomID: String? = null
    @ColumnInfo(name = "text") var text: String = ""
    @ColumnInfo(name = "text_html") var textHTML: String? = null
    @ColumnInfo(name = "createdAt") var createdAt: Date? = Date(0)

    @ColumnInfo(name = "sending_in_process") var sendingInProcess: Boolean = false

    @ColumnInfo(name = "image") var image: String = ""
    @ColumnInfo(name = "image_width") var imageWidth: Int? = null
    @ColumnInfo(name = "image_height") var imageHeight: Int? = null

    val status: String
        get() = "Sent"

    fun getTimeFormatted(): String{
        val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
        return formatter.format(this.createdAt)
    }

    @Ignore
    fun getTextHTMLAsSpanned(): Spanned? {
        if (textHTML == null) {
            return null
        }
        return HtmlCompat.fromHtml(textHTML!!, HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    @Ignore
    fun getTextAsSpanned(): Spanned? {
        if (text == "") {
            return null
        }
        return HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
    }
}