package blog.nordgedanken.simplematrix.data.view

import androidx.room.Embedded
import androidx.room.Relation
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import java.util.*

/**
 * Created by MTRNord on 15.10.2018.
 */
class RoomFull {
  @Embedded
  var room: Room? = null

  @Relation(parentColumn = "id",
      entityColumn = "room_id")
  var messages: List<Message> = ArrayList()

  @Relation(parentColumn = "id",
      entityColumn = "room_id")
  var usersDB: List<User> = ArrayList()

  val dialogPhoto: String
    get() {
      if (room?.dialogPhoto?.isEmpty()!! && usersDB.isNotEmpty() && usersDB.count() <= 2) {
        val directPartnerAvatar = getDirectPartnerAvatar()
        if (directPartnerAvatar != "" && directPartnerAvatar != null) {
          return directPartnerAvatar
        }
      }
      return room?.dialogPhoto!!
    }

  private fun getFullMessages(): List<MessageFull> {
    val fullMessages = arrayListOf<MessageFull>()
    for (message in messages) {
      val messageFull = MessageFull()
      messageFull.message = message
      val userDBL = usersDB.filter { u -> u.MXID == message.userID }
      messageFull.usersDB = userDBL
      fullMessages.add(messageFull)
    }

    return fullMessages
  }

  private fun getDirectPartnerAvatar(): String? {
    for (user in usersDB) {
      if (user.MXID != MatrixClient.id) {
        return user.avatar
      }
    }
    return null
  }

  fun getUnreadCount(): Int {
    return room?.unreadCount!!
  }

  fun getUsers(): MutableList<User> {
    return usersDB.toMutableList()
  }

  val lastMessage: MessageFull?
    get() {
      if (getFullMessages().isEmpty()) {
        /*val message = Message()
        val lastMessageFull = MessageFull()
        lastMessageFull.message = message
        val userDBL = mutableListOf<User>()
        for (user in usersDB) {
            if (user.MXID == message.userID) userDBL.add(user)
        }
        lastMessageFull.usersDB = userDBL
        return lastMessageFull*/
        return null
      }
      val latestMessageL = getFullMessages().asSequence().sortedByDescending { it.message?.createdAt }.first()
      val lastMessageFull = MessageFull()
      lastMessageFull.message = latestMessageL.message
      val userDBL = usersDB.filter { u -> u.MXID == latestMessageL.message?.userID }
      lastMessageFull.usersDB = userDBL
      return lastMessageFull
    }

  val dialogName: String
    get() {
      if (room?.dialogName?.isNotEmpty()!!) {
        return room?.dialogName!!
      } else if (usersDB.isNotEmpty()) {
        val directPartnerName = getDirectPartnerName()
        if (directPartnerName != null) {
          return directPartnerName
        }
      } else if (room?.canonicalAlias?.isNotEmpty()!!) {
        return room?.canonicalAlias!!
      } else if (room?.aliases?.isNotEmpty()!!) {
        return room?.getFirstAlias()!!
      }
      return room?.id!!
    }


  private fun getDirectPartnerName(): String? {
    for (user in usersDB) {
      if (user.MXID != MatrixClient.id) {
        return user.name
      }
    }
    return null
  }
}