/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import blog.nordgedanken.simplematrix.utils.db.BaseDao

/**
 * Created by MTRNord on 01.09.2018.
 */
@Dao
abstract class UserDao : BaseDao<User> {
    @Transaction
    @Query("SELECT * FROM users")
    abstract fun all(): List<User>

    @Transaction
    @Query("SELECT * FROM users WHERE room_id = :roomID")
    abstract fun allByRooom(roomID: String): List<User>

    @Transaction
    @Query("SELECT * FROM users WHERE mxid = :mxid")
    abstract fun allByMXID(mxid: String): List<User>

    @Transaction
    @Query("SELECT * FROM users WHERE room_id = :roomID AND mxid = :mxid LIMIT 1")
    abstract fun userForMessage(roomID: String, mxid: String): User

    @Transaction
    @Query("SELECT * FROM users WHERE id IN (:userIds)")
    abstract fun loadAllByIds(userIds: IntArray): List<User>

    @Transaction
    @Query("DELETE FROM users")
    abstract fun nukeTable()
}
