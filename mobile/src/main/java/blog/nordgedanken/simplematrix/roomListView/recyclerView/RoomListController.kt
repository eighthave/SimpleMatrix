package blog.nordgedanken.simplematrix.roomListView.recyclerView

import android.content.Context
import blog.nordgedanken.matrix_android_sdk.api.json.ChunkItem
import com.airbnb.epoxy.TypedEpoxyController

/**
 * Created by MTRNord on 10.02.2019.
 */
class RoomListController(val context: Context) : TypedEpoxyController<List<ChunkItem?>>() {
    override fun buildModels(data: List<ChunkItem?>?) {
        for ((i, item) in data?.withIndex()!!) {
            RoomListItem_()
                    .id(i)
                    .addTo(this)
        }
    }
}