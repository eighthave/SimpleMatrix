package blog.nordgedanken.simplematrix.roomListView.recyclerView

import android.view.View
import blog.nordgedanken.simplematrix.R
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder

/**
 * Created by MTRNord on 10.02.2019.
 */
@EpoxyModelClass(layout = R.layout.public_rooms_list_item)
abstract class RoomListItem : EpoxyModelWithHolder<RoomListItem.Holder>() {


    override fun bind(holder: Holder) {
    }

    class Holder : EpoxyHolder() {
        override fun bindView(itemView: View) {
        }
    }
}