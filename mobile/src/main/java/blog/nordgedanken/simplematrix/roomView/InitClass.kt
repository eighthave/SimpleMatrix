package blog.nordgedanken.simplematrix.roomView

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.viewpager.widget.ViewPager
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.SettingsActivity
import blog.nordgedanken.simplematrix.State
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.roomView.tabFragments.ChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.DirectChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.FavsFragment
import blog.nordgedanken.simplematrix.utils.ViewPagerAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.orhanobut.logger.Logger
import org.jetbrains.anko.doAsync
import org.koin.android.ext.android.inject

abstract class InitClass : AppCompatActivity() {
    private val adapter: ViewPagerAdapter by lazy { ViewPagerAdapter(supportFragmentManager) }

    private val state by inject<State>()

    //This is our viewPager
    val viewPager: ViewPager by lazy { findViewById<ViewPager>(R.id.viewpager) }

    private val tabIcons = intArrayOf(
            R.drawable.ic_star_black_24dp,
            R.drawable.ic_chat_black_24dp,
            R.drawable.ic_person_black_24dp
    )

    // Fragments
    private val chatsFragment by inject<ChatsFragment>()
    private val favsFragment by inject<FavsFragment>()
    private val directChatsFragment by inject<DirectChatsFragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (MatrixClient.client !== null) {
            if (!state.loadingDone) {
                doAsync {
                    MatrixClient().loadCache(this@InitClass)
                    state.loadingDone = true
                }
            }
        } else {
            findViewById<TextView>(R.id.sync_status)?.text = getString(R.string.error, "Login failed")
            findViewById<TextView>(R.id.sync_status)?.setTextColor(Color.RED)
        }
    }

    protected fun setupTabs(tabLayout: TabLayout, viewPager: ViewPager) {
        //Initializing viewPager
        setupViewPager(viewPager)

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)?.setIcon(tabIcons[0])
        tabLayout.getTabAt(1)?.setIcon(tabIcons[1])
        tabLayout.getTabAt(2)?.setIcon(tabIcons[2])

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                // RESET SEARCH
                val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.currentItem)

                when (fragment) {
                    is FavsFragment -> {
                        fragment.search("")
                    }
                    is DirectChatsFragment -> {
                        fragment.search("")
                    }
                    is ChatsFragment -> {
                        fragment.search("")
                    }
                    else -> {
                    }
                }
            }

        })
        val tab = tabLayout.getTabAt(1)
        tab?.select()
    }

    protected fun setupFab(viewPager: ViewPager) {
        val fab = findViewById<FloatingActionButton>(R.id.create_chat)
        fab.setOnClickListener { view ->
            val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.currentItem)
            // Todo make it actually do real stuff
            when (fragment) {
                is FavsFragment -> {
                    fragment.addDialog()
                }
                is DirectChatsFragment -> {
                    fragment.addDialog()
                }
                is ChatsFragment -> {
                    fragment.addDialog()
                }
                else -> Snackbar.make(view, "Unable to add Dialog", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter.addFragment(2, directChatsFragment, resources.getString(R.string.tab_direct_chats))
        adapter.addFragment(1, chatsFragment, resources.getString(R.string.tab_chats))
        adapter.addFragment(0, favsFragment, resources.getString(R.string.tab_favorites))
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        val searchItem = menu.findItem(R.id.action_search)

        val searchView = searchItem.actionView as SearchView

        searchItem.setOnActionExpandListener(object  : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                Logger.d("RESET")
                // RESET SEARCH
                val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.currentItem)

                return when (fragment) {
                    is FavsFragment -> {
                        fragment.search("")
                        true
                    }
                    is DirectChatsFragment -> {
                        fragment.search("")
                        true
                    }
                    is ChatsFragment -> {
                        fragment.search("")
                        true
                    }
                    else -> false
                }
            }
        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null && query != "") {
                    val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.currentItem)

                    return when (fragment) {
                        is FavsFragment -> {
                            fragment.search(query)
                            true
                        }
                        is DirectChatsFragment -> {
                            fragment.search(query)
                            true
                        }
                        is ChatsFragment -> {
                            fragment.search(query)
                            true
                        }
                        else -> false
                    }
                }
                return false
            }

        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, SettingsActivity::class.java)
            // Check if we're running on Android 5.0 or higher
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Apply activity transition
                startActivity(intent,
                        ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
            } else {
                // Swap without transition
                startActivity(intent,
                        null)
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}