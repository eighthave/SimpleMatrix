package blog.nordgedanken.simplematrix.roomView.recyclerView

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import blog.nordgedanken.simplematrix.chatView.ChatRoom
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.roomView.recyclerView.items.RoomLayoutElement_
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder


class PagingRoomController(val context: Context) : PagedListEpoxyController<RoomFull>() {
    var filterValue = ""
    override fun buildItemModel(currentPosition: Int, item: RoomFull?): EpoxyModel<*> {
        return if (item == null) {
            RoomLayoutElement_()
                    .id(-currentPosition)
        } else {
            val element = RoomLayoutElement_()

            element.id(item.room?.id)
                    .clickListener { _ ->
                        Sentry.getContext().recordBreadcrumb(
                                BreadcrumbBuilder().setMessage("User is going to open a ChatRoom").build()
                        )
                        val intent = Intent(context, ChatRoom::class.java).putExtra("roomID", item.room?.id)
                        // Swap without transition
                        startActivity(context, intent,
                                null)
                    }
            element.dialogName = item.dialogName
            if (item.lastMessage != null) {
                element.dialogDate = item.lastMessage?.message?.getTimeFormatted()!!
                element.lastMessage = item.lastMessage?.message?.getTextAsSpanned()
            }
            element.roomImageURL = item.dialogPhoto
            element.room = item
            element
        }
    }

    override fun addModels(models: List<EpoxyModel<*>>) {
        if (filterValue != "") {
            val filteredModels = mutableListOf<EpoxyModel<*>>()
            for (model in models) {
                val modelL = model as RoomLayoutElement_
                if (modelL.room != null && modelL.dialogName != null) {
                    if (
                            modelL.dialogName?.contains(filterValue, true)!! ||
                            modelL.room?.room?.canonicalAlias?.contains(filterValue, true)!!
                    ) {
                        filteredModels.add(modelL)
                    } else {
                        val aliases = modelL.room?.room?.aliases
                        if (aliases?.isNotEmpty()!!) {
                            for (alias in aliases) {
                                if (alias.contains(filterValue, true)) {
                                    filteredModels.add(modelL)
                                }
                            }
                        }
                    }
                }
            }
            super.addModels(filteredModels)
        } else {
            super.addModels(models)
        }
    }

    init {
        isDebugLoggingEnabled = true
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        throw exception
    }
}