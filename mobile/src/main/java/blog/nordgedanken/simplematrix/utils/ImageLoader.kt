package blog.nordgedanken.simplematrix.utils

import android.content.Context
import android.widget.ImageView
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.data.view.User
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread

/**
 * Handles different types of Image Loading
 *
 * @author Marcel Radzio
 *
 */
class ImageLoader(val context: Context, val glide: RequestManager) {

    /**
     * Loads images for entities of type [User].
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     * @param user The [User] entity
     *
     */
    fun loadImage(imageView: ImageView, url: String, user: User) {
        doAsync {
            val name = user.name

            var requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()

            if (user.nameAvatar == null) {
                user.nameAvatar = getNameAvatar(name)
            }

            requestOptions = requestOptions.placeholder(user.nameAvatar)

            if (url.startsWith("mxc://")) {
                context.runOnUiThread {
                    glide.load(MatrixClient.client?.getMedia(url)?.permaLink?.toString())
                            .apply(requestOptions)
                            .into(imageView)
                }
            } else {
                context.runOnUiThread {
                    imageView.setImageDrawable(user.nameAvatar)
                }
            }
        }
    }

    /**
     * Loads images for entities of type [RoomFull].
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     * @param dialog The [RoomFull] entity
     *
     */
    fun loadImage(imageView: ImageView, url: String, dialog: RoomFull) {
        doAsync {
            val name = dialog.dialogName
            var requestOptions = RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()

            if (dialog.room?.nameAvatar == null) {
                dialog.room?.nameAvatar = getNameAvatar(name)
            }


            requestOptions = requestOptions.placeholder(dialog.room?.nameAvatar)

            if (url.startsWith("mxc://")) {
                context.runOnUiThread {
                    glide.load(MatrixClient.client?.getMedia(url)?.permaLink?.toString())
                            .apply(requestOptions)
                            .into(imageView)
                }
            } else {
                context.runOnUiThread {
                    imageView.setImageDrawable(dialog.room?.nameAvatar)
                }
            }
        }
    }

    /**
     * Loads images from a MXC URL.
     * This happens async
     *
     * @param imageView The target where the image should get loaded into.
     *
     * @param url The MXC URL from the Matrix Network that should get loaded into the imageView
     *
     */
    fun loadImage(imageView: ImageView, url: String) {
        doAsync {
            if (url.startsWith("mxc://")) {
                val requestOptions = RequestOptions()
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transforms(RoundedCorners(10))
                context.runOnUiThread {
                    glide.load(MatrixClient.client?.getMedia(url)?.permaLink?.toString())
                            .apply(requestOptions)
                            .into(imageView)
                }
            }
        }
    }

    /**
     * Generates a generic [NameAvatar] based on either the RoomID, UserID or alias.
     *
     * @param name The Name of either a Room or User
     *
     */
    private fun getNameAvatar(name: String): NameAvatar {
        return if (name.count() > 1 && name.startsWith("!")) {
            NameAvatar(context, name.trimStart('!')[0].toString().toUpperCase())
        } else if (name.count() > 1 && name.startsWith("@")) {
            NameAvatar(context, name.trimStart('@')[0].toString().toUpperCase())
        } else if (name.count() > 1 && name.startsWith("#")) {
            NameAvatar(context, name.trimStart('#')[0].toString().toUpperCase())
        } else if (name.count() > 1) {
            NameAvatar(context, name[0].toString().toUpperCase())
        } else {
            NameAvatar(context, "")
        }
    }

}