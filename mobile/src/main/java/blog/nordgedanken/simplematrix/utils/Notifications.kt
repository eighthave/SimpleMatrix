/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.Person
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.graphics.drawable.IconCompat
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.chatView.ChatRoom
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import com.orhanobut.logger.Logger
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


/**
 * Created by MTRNord on 09.08.2018.
 */
object Notification : KoinComponent {
    const val DirectChatsChannelID = "directChat"
    const val FavChatsChannelID = "favChat"
    const val ChatsChannelID = "Chat"
    const val LISTENING_FOR_EVENTS_NOTIFICATION = "LISTEN_FOR_EVENTS_NOTIFICATION"
    private var lastBackgroundSyncID: Int? = null
    private val mNotificationManager: NotificationManager by inject()

    fun create(context: Context, room: RoomFull, messages: List<MessageFull>) {
        val channel = when (room.room?.tag) {
            "direct" -> Notification.DirectChatsChannelID
            "favs" -> Notification.FavChatsChannelID
            "chats" -> Notification.ChatsChannelID
            else -> null
        }

        val notificationId = room.room?.notificationID

        //TODO FIX. THIS NEVER WORKS! ALWAYS ENTERS ELSE
        if (notificationId !== null) {
/*
            // Extract MessagingStyle object from the active notification.
            val activeStyle = NotificationCompat.MessagingStyle.extractMessagingStyleFromNotification(notificationL.notification)

            // Recover builder from the active notification.


            val user = Person.Builder()
                    //.setIcon()
                    .setName(MatrixClient.name).build()

            // The recoveredBuilder is Notification.Builder whereas the activeStyle is NotificationCompat.MessagingStyle.
            // It means you need to recreate the style as Notification.MessagingStyle to make it compatible with the builder.
            val newStyle = NotificationCompat.MessagingStyle(user)
                    .setConversationTitle(room.dialogName)
                    .setGroupConversation(true)
            newStyle.conversationTitle = activeStyle?.conversationTitle
            for (it in activeStyle?.messages!!) {
                newStyle.addMessage(NotificationCompat.MessagingStyle.Message(it.text, it.timestamp, it.person))
            }

            for (message in messages) {
                newStyle.addMessage(message.text, message.createdAt.time, message.user.getPerson())
            }

            // Set the new style to the recovered builder.
            notificationL.setStyle(newStyle)

            // Update the active notification.
            NotificationManagerCompat.from(context).notify(notificationId, notificationL.build())
            */
        } else {
            val iconCompat = IconCompat.createWithContentUri(MatrixClient.avatar_url)
            val user = Person.Builder()
                    .setIcon(iconCompat)
                    .setName(MatrixClient.name).build()
            val style = NotificationCompat.MessagingStyle(user)
                    .setConversationTitle(room.dialogName)
                    .setGroupConversation(true)

            for (message in messages) {
                style.addMessage(message.message?.text, message.message?.createdAt?.time!!, message.getUser().getPerson())
            }

            val intent = Intent(context, ChatRoom::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }.putExtra("roomID", room.room?.id).setAction(System.currentTimeMillis().toString())

            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val mBuilder = NotificationCompat.Builder(context, channel!!)
                    .setSmallIcon(R.drawable.ic_notification_logo)
                    .setContentTitle("${messages.count()} new message(s) in ${room.dialogName}")
                    .setContentText("test")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setStyle(style)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)

            if (room.room?.notificationID == null) {
                val unixTime = System.currentTimeMillis() / 1000L
                room.room?.notificationID = unixTime.toInt()
            }

            val notificationManager = NotificationManagerCompat.from(context)
            notificationManager.notify(room.room?.notificationID!!, mBuilder.build())
        }
    }

    fun createNotificationChannel(context: Context, channelID: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var name: String? = null
            var description: String? = null
            var importance: Int? = null
            when (channelID) {
                DirectChatsChannelID -> {
                    name = context.getString(R.string.channel_direct_name)
                    description = context.getString(R.string.channel_direct_description)
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                }
                FavChatsChannelID -> {
                    name = context.getString(R.string.channel_favs_name)
                    description = context.getString(R.string.channel_favs_description)
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                }
                ChatsChannelID -> {
                    name = context.getString(R.string.channel_chats_name)
                    description = context.getString(R.string.channel_chats_description)
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                }
                LISTENING_FOR_EVENTS_NOTIFICATION -> {
                    name = context.getString(R.string.channel_foreground_service_notification)
                    description = context.getString(R.string.channel_foreground_service_notification_description)
                    importance = NotificationManager.IMPORTANCE_MIN
                }
            }
            val channel = NotificationChannel(channelID, name!!, importance!!)
            channel.description = description!!
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(context, NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    fun initialSync(context: Context): Int {
        val notification = buildForegroundServiceNotification(context, R.string.initialSync)

        val notificationManager = NotificationManagerCompat.from(context)
        val unixTime = System.currentTimeMillis()
        notificationManager.notify(unixTime.toInt(), notification)
        return unixTime.toInt()
    }

    fun removeInitialSync(context: Context, lastInitialSyncID: Int) {
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.cancel(lastInitialSyncID)
    }

    fun backgroundSync(context: Context): Int {

        val notification = buildForegroundServiceNotification(context, R.string.backgroundSync)

        val notificationManager = NotificationManagerCompat.from(context)
        val unixTime = System.currentTimeMillis()
        lastBackgroundSyncID = unixTime.toInt()
        notificationManager.notify(unixTime.toInt(), notification)
        return unixTime.toInt()
    }

    /**
     * Build a polling thread listener notification
     *
     * @param context       Android context
     * @param subTitleResId subtitle string resource Id of the notification
     * @return the polling thread listener notification
     */
    fun buildForegroundServiceNotification(context: Context, @StringRes subTitleResId: Int): android.app.Notification {
        // build the pending intent go to the home screen if this is clicked.
        val i = Intent(context, RoomsActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pi = PendingIntent.getActivity(context, 0, i, 0)

        val builder = NotificationCompat.Builder(context, LISTENING_FOR_EVENTS_NOTIFICATION)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(context.getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentText(context.getString(subTitleResId))
                .setSmallIcon(R.drawable.ic_notification_logo)
                .setContentIntent(pi)


        val notification = builder.build()

        notification.flags = notification.flags or NotificationCompat.FLAG_NO_CLEAR

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // some devices crash if this field is not set
            // even if it is deprecated

            // setLatestEventInfo() is deprecated on Android M, so we try to use
            // reflection at runtime, to avoid compiler error: "Cannot resolve method.."
            try {
                val deprecatedMethod = notification.javaClass
                        .getMethod("setLatestEventInfo", Context::class.java, CharSequence::class.java, CharSequence::class.java, PendingIntent::class.java)
                deprecatedMethod.invoke(notification, context, context.getString(R.string.app_name), context.getString(subTitleResId), pi)
            } catch (ex: Exception) {
                Logger.e("buildNotification(): Exception - setLatestEventInfo() Msg=" + ex.message, ex)
            }

        }

        return notification
    }

    @SuppressLint("NewApi")
    fun notificationStillVisible(id: Int): Boolean {
        val notifications = mNotificationManager.activeNotifications
        for (notification in notifications) {
            if (notification.id == id) {
                return true
            }
        }
        return false
    }
}