/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.User
import java.util.*

class UserDataListAdapter(private val context: Context, private val groups: ArrayList<HeaderInfo>) : BaseExpandableListAdapter() {

  override fun getChild(groupPosition: Int, childPosition: Int): Any {
    val child = groups[groupPosition]
    return child.user!!
  }

  override fun getChildId(groupPosition: Int, childPosition: Int): Long {
    return childPosition.toLong()
  }

  override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean,
                            view: View?, parent: ViewGroup): View {
    var viewL = view

    //TODO add logic for all views
    val detailInfo = getChild(groupPosition, childPosition) as User
    if (groups[groupPosition].view == null || groups[groupPosition].view !== viewL) {
      val name = groups[groupPosition].name
      viewL = when (name) {
        "Devices" -> {
          val infalInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
          infalInflater.inflate(R.layout.user_profile_devices, null)

          // TODO generate Cards
          //val deviceCards = viewL!!.findViewById<View>(R.id.deviceCards) as CardView
        }
        "Direct-Chats" -> {
          val infalInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
          infalInflater.inflate(R.layout.user_profile_direct_chats, null)

          // TODO generate Cards
          //val directChatCards = viewL!!.findViewById<View>(R.id.directChatCards) as CardView

        }
        "User-Options" -> {
          val infalInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
          infalInflater.inflate(R.layout.user_profile_user_options, null)
        }
        /*else -> {

            val infalInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            infalInflater.inflate(R.layout.child_row, null)

        }*/
        // TODO make it something else
        else -> null
      }

    }

    groups[groupPosition].view = viewL

    return viewL!!
  }

  override fun getChildrenCount(groupPosition: Int): Int {
    return 1
  }

  override fun getGroup(groupPosition: Int): Any {
    return groups[groupPosition]
  }

  override fun getGroupCount(): Int {
    return groups.size
  }

  override fun getGroupId(groupPosition: Int): Long {
    return groupPosition.toLong()
  }

  override fun getGroupView(groupPosition: Int, isLastChild: Boolean, view: View?,
                            parent: ViewGroup): View {
    var viewL = view
    val headerInfo = getGroup(groupPosition) as HeaderInfo
    if (viewL == null) {
      val inf = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
      viewL = inf.inflate(R.layout.user_profile_group, null)
    }

    val heading = viewL!!.findViewById<View>(R.id.heading) as TextView
    val name = headerInfo.name
    heading.text = name

    return viewL
  }

  override fun hasStableIds(): Boolean {
    return true
  }

  override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
    return true
  }
}