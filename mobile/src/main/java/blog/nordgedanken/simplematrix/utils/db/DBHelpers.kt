package blog.nordgedanken.simplematrix.utils.db

import android.database.sqlite.SQLiteConstraintException
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.view.*
import com.orhanobut.logger.Logger
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * Helps to do common Database tasks by minifying boilerplate code in other classes.
 *
 * @author Marcel Radzio
 *
 */
object DBHelpers : KoinComponent {
    val db: AppDatabase by inject()

    fun doesRoomExistInDB(roomID: String): Boolean {
        return db.roomDao().exists(roomID)
    }

    fun removeRoomFromDB(roomID: String) {
        db.roomDao().deleteByID(roomID)
    }

    /**
     *
     * Saves an [ArrayList] of [RoomFull] to the Database.
     * It also handles to check if they are new Rooms or existing ones.
     *
     */
    fun saveRoomToDB(rooms: List<RoomFull>) {
        val workMapDB = db.roomDao().allIDs()
        val new: Iterable<Room> = rooms.asSequence().filter { it.room?.id !in workMapDB }.mapNotNull { it.room }.asIterable()
        val update: Iterable<Room> = rooms.asSequence().filter { it.room?.id in workMapDB }.mapNotNull { it.room }.asIterable()
        if (new.firstOrNull() != null) db.roomDao().insertAll(new)
        if (update.firstOrNull() != null) db.roomDao().updateAll(update)
    }

    /**
     *
     * Saves an [List] of [User] to the Database.
     * It also handles to check if they are new Users or existing ones.
     *
     */
    fun saveUserToDB(users: List<User>) {
        val new: Iterable<User> = users.asSequence().filter { it.id == null }.asIterable()
        val update: Iterable<User> = users.asSequence().filter { it.id != null }.asIterable()
        if (new.firstOrNull() != null) db.userDao().insertAll(new)
        if (update.firstOrNull() != null) db.userDao().updateAll(update)
    }

    /**
     *
     * Saves an [List] of [MessageFull] to the Database.
     * It also handles to check if they are new Messages or existing ones.
     *
     */
    fun saveMessagesToDB(messages: List<MessageFull>) {
        val workMapDB = db.messageDao().allIDs()
        val new: Iterable<Message> = messages.asSequence().filter { it.message?.id !in workMapDB }.mapNotNull { it.message }.asIterable()
        val update: Iterable<Message> = messages.asSequence().filter { it.message?.id in workMapDB }.mapNotNull { it.message }.asIterable()

        try {
            db.messageDao().insertAll(new)
        } catch (e: SQLiteConstraintException) {
            Logger.e("Failed to save new Message to DB: ${e.message}")
        }

        try {
            db.messageDao().updateAll(update)
        } catch (e: SQLiteConstraintException) {
            Logger.e("Failed to update Message to DB: ${e.message}")
        }

    }

    fun getMessagesByRoomIDFromDB(roomID: String): List<MessageFull?> {
        return db.messageDao().loadAllByRoomId(roomID)
    }


    fun getMessageCountByRoomIDFromDB(roomID: String): Int {
        return db.messageDao().countByRoomID(roomID)
    }

    /**
     * Changes the ID of a Message in the Database
     */
    fun updateMessageIDInDB(oldID: String, newID: String) {
        db.messageDao().updateMessageID(oldID, newID)
    }

    /**
     * Changes the sending status of a Message in the Database
     */
    fun updateMessageSendingStatusInDB(id: String, status: Boolean) {
        db.messageDao().updateMessageSendingStatus(id, status)
    }

    /**
     *
     * Checks if a room does already exist in the DB
     *
     * @return A [Boolean] indicating if it exists
     *
     */
    fun roomExistsInCache(roomID: String): Boolean {
        var exists = false
        val room = db.roomDao().roomIDByID(roomID)
        if (room == roomID) {
            exists = true
        }
        return exists
    }

    /**
     *
     * Gets a room by it's ID
     *
     * @return [RoomFull]
     *
     */
    fun getRoomByID(roomID: String): RoomFull {
        return db.roomDao().roomByID(roomID)!!
    }

    /**
     *
     * Gets Users from the cache.
     * @return a [List] of [User]
     *
     */
    fun getUsersFromCache(): List<User> {
        return db.userDao().all()
    }

    /**
     *
     * Gets Users from the cache.
     * @return a [List] of [User]
     *
     */
    fun getUserByMXIDRoomIDFromCache(userID: String, roomID: String): User {
        return db.userDao().userForMessage(roomID, userID)
    }

    /**
     *
     * Gets Users from the cache by the attached roomID.
     * @return a [List] of [User]
     */
    fun getUsersByRoomIDFromCache(roomID: String): List<User> {
        return db.userDao().allByRooom(roomID)
    }
}